package fe.controllers;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fe.entities.Currency;

@Controller
public class MainController {
  private final static Logger LOGGER = LoggerFactory.getLogger(MainController.class);
  
  @Value("${backend.hostname}")  
  private String host;
  @Value("${backend.port}")  
  private int port;
  @Value("${server.port}")  
  private int serverPort;
  @Autowired
  private String thisHostName;


  private List<Currency> requestProcessedData(String endPoint) {
    String URL = "http://" + host + ":" + port;
    List<Currency> exchangeInJson = Collections.emptyList();
    ObjectMapper mapper = new ObjectMapper();
    try {
        exchangeInJson = mapper.readValue(new URL(URL + endPoint).openConnection().getInputStream(), new TypeReference<List<Currency>>(){ });
    } catch (IOException e) {
      LOGGER.error("Cannot obtain list", e);
    }
    return exchangeInJson;
  }
  
  @GetMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
  public String indexWeb(Model model) {
    ZonedDateTime zdt = ZonedDateTime.now(ZoneId.of("Europe/Kiev"));
    model.addAttribute("date", DateTimeFormatter.ISO_ZONED_DATE_TIME.withLocale(Locale.forLanguageTag("uk")).format(zdt));
    model.addAttribute("currencies", requestProcessedData("/readDataForSale/"));
    return "index";
  }
  
  @GetMapping(value = "/readDataForSale/{cc}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public @ResponseBody ResponseEntity<Currency> requestCcDataForSale(@PathVariable String cc) { 
    Currency currency = requestProcessedData("/readDataForSale/")
        .stream()
        .filter(c -> c.getCc().equalsIgnoreCase(cc))
        .findAny()
        .get();
    return new ResponseEntity<Currency>(currency, HttpStatus.OK);
  }
  
  @GetMapping(value = "/readDataForBuy/{cc}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public @ResponseBody ResponseEntity<Currency> requestCcDataForBuy(@PathVariable String cc) {    
    Currency currency = requestProcessedData("/readDataForBuy/")
        .stream()
        .filter(c -> c.getCc().equalsIgnoreCase(cc))
        .findAny()
        .get();
    return new ResponseEntity<Currency>(currency, HttpStatus.OK);
  }
  

  @GetMapping(value = "/sale/{cc}", produces = MediaType.APPLICATION_XHTML_XML_VALUE)
  public String requestCcDataForSaleWeb(@PathVariable String cc, Model model) { 
    Currency currency = requestProcessedData("/readDataForSale/")
        .stream()
        .filter(c -> c.getCc().equalsIgnoreCase(cc))
        .findAny()
        .get();
    model.addAttribute("currency", currency);
    model.addAttribute("hostname", thisHostName);
    model.addAttribute("port", serverPort);
    model.addAttribute("op", "sale");
    return "currency";
  }
  
  @GetMapping(value = "/buy/{cc}", produces = MediaType.APPLICATION_XHTML_XML_VALUE)
  public String requestCcDataForBuyWeb(@PathVariable String cc, Model model) {    
    Currency currency = requestProcessedData("/readDataForBuy/")
        .stream()
        .filter(c -> c.getCc().equalsIgnoreCase(cc))
        .findAny()
        .get();
    model.addAttribute("currency", currency);
    model.addAttribute("hostname", thisHostName);
    model.addAttribute("port", serverPort);
    model.addAttribute("op", "buy");
    
    return "currency";
  }


  
  @GetMapping(value = "/readDataForSale/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public @ResponseBody ResponseEntity<List<Currency>> requestAllCurrenciesForSale() {    
    return new ResponseEntity<List<Currency>>(requestProcessedData("/readDataForSale/"), HttpStatus.OK);
  }

}
